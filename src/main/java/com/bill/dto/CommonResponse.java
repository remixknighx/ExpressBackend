package com.bill.dto;

/**
 * @author wangjf
 * @date 2018/1/7 0007.
 */
public class CommonResponse<T> {

    private boolean success;

    private String error;

    private T data;

    public CommonResponse(){
        this.success = true;
    }

    public CommonResponse(T data) {
        this.success = true;
        this.data = data;
    }

    public CommonResponse(boolean success, String error) {
        this.success = success;
        this.error = error;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
