package com.bill.chain;

/**
 * @author wangjf
 * @date 2018/6/3 0003.
 */
public abstract class ChainHandler {

    public void execute(Chain chain){
        handleProcess();
        chain.proceed();
    }

    protected abstract void handleProcess();

}
