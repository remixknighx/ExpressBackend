package com.bill.aspect;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @author wangjf
 * @date 2018/6/2 0002.
 */
@Aspect
@Component
public class AspectConfig {

    @Pointcut("execution(public * com.bill.service.impl.*ServiceImpl.*(..))")
    public void matchType(){}

    @Before("matchType()")
    public void before(){
        System.out.println("################## before ############################");
    }

    @After("matchType()")
    public void after(){
        System.out.println("################## after ############################");
    }

    @Around("matchType()")
    public Object around(ProceedingJoinPoint joinPoint){
        Object result = null;
        try {
            result = joinPoint.proceed(joinPoint.getArgs());
            System.out.println("### after returning ");
        }catch (Throwable e){
            System.out.println("### ex ");
            e.printStackTrace();
        }finally {
            System.out.println("### finally ");
        }
        return result;
    }

}
