package com.bill.dao;

import com.bill.entity.Student;

/**
 * @author wangjf
 * @date 2018/1/7 0007.
 */
public interface TestDao {

    Integer insertStudent(Student student);

    Integer updateStudent(Student student);

    Student queryStudentByName(String name);

    Student queryStudentById(Long id);

}
