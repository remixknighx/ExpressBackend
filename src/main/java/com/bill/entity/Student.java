package com.bill.entity;

import java.util.Date;

/**
 * @author wangjf
 * @date 2018/1/7 0007.
 */
public class Student {

    private long id;

    private String name;

    private int gender;

    private boolean status;

    private Date createTime;

    public long getId() {
        return id;
    }

    public Student setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Student setName(String name) {
        this.name = name;
        return this;
    }

    public int getGender() {
        return gender;
    }

    public Student setGender(int gender) {
        this.gender = gender;
        return this;
    }

    public boolean isStatus() {
        return status;
    }

    public Student setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Student setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender=" + gender +
                ", status=" + status +
                ", createTime=" + createTime +
                '}';
    }
}
