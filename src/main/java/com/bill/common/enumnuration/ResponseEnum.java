package com.bill.common.enumnuration;

/**
 * @author wangjf
 * @date 2018/1/20 0020.
 */
public enum ResponseEnum {
    SUCCESS_RESP("SUCCESS", "请求成功"),
    ERROR_RESP("ERROR", "请求失败");

    private String key;
    private String value;

    private ResponseEnum(String key, String value){
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
