package com.bill.common.exception;

/**
 * @author wangjf
 * @date 2018/1/20 0020.
 */
public class TestException extends RuntimeException {

    private String message;

    private Throwable cause;

    public TestException(){}

    public TestException(String message){
        super(message);
    }

    public TestException(Throwable cause){
        super(cause);
    }

    public TestException(String message, Throwable cause){
        super(message, cause);
    }

}
