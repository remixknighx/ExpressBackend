package com.bill.common.constant;

/**
 * @author wangjf
 * @date 2018/3/18 0018.
 */
public class HttpConstant {

    public static final String X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded;charset=UTF-8";

    public static final String JSON = "application/json;charset=UTF-8";

    public static final String UTF_8 = "UTF-8";

}
