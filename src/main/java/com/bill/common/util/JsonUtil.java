package com.bill.common.util;

import com.alibaba.fastjson.JSONObject;
import com.bill.entity.Student;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author wangjf
 * @date 2018/3/25 0025.
 */
public class JsonUtil {

    private static Gson gson = new Gson();

    public static String convertList2Json(List<?> list){
        return gson.toJson(list);
    }

    public static List<?> convertJson2List(String json){
        return gson.fromJson(json, List.class);
    }

    public static String convertMap2Json(Map<?, ?> map){
        return gson.toJson(map);
    }

    public static Map<?, ?> convertJson2Map(String json){
        return gson.fromJson(json, Map.class);
    }

    public static String convertObject2Json(Object object){
        return gson.toJson(object);
    }

    public static Object convertJson2Object(String json){
        return gson.fromJson(json, Object.class);
    }

}
