package com.bill.common.util;

import com.bill.common.constant.HttpConstant;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.Request;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author wangjf
 * @date 2018/3/17 0017.
 */
public class HttpUtil {

    private static Logger logger = LoggerFactory.getLogger(HttpUtil.class);

    /**
     * 调用HTTP get方法获取数据
     *
     * @param url url地址
     */
    public static String getResult(String url) throws IOException {
        String response = "";
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse response1 = null;
        try {
            response1 = httpclient.execute(httpGet);
            HttpEntity entity = response1.getEntity();

            if(entity != null){
                response = EntityUtils.toString(entity);
            }
            EntityUtils.consume(entity);

        } catch (IOException e) {
            logger.error("--- Http Get Error: ---", e);
        } finally {
            response1.close();
            return response;
        }
    }

    /**
     * 调用HTTP post方法获取数据，默认提交格式为x-www-form-urlencoded
     *
     * @param url
     * @param paramMap
     * */
    public static String postFormParam(String url, Map<String, Object> paramMap) throws IOException {
        // 设置参数
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        for (Map.Entry<String, Object> keyValue: paramMap.entrySet()){
            nvps.add(new BasicNameValuePair(keyValue.getKey(), String.valueOf(keyValue.getValue())));
        }

        return postResult(url, new UrlEncodedFormEntity(nvps, HttpConstant.UTF_8), HttpConstant.X_WWW_FORM_URLENCODED);
    }

    public static String postJsonResult(String url, String jsonParam) throws IOException{
        StringEntity stringEntity = new StringEntity(jsonParam, HttpConstant.UTF_8);
        stringEntity.setContentEncoding(HttpConstant.UTF_8);
        stringEntity.setContentType("application/json");
        return postResult(url, stringEntity, HttpConstant.JSON);
    }

    /**
     * 调用HTTP post方法获取数据
     *
     * @param url
     * @param paramEntity
     * @param contentType application/x-www-form-urlencoded;utf-8
     * */
    public static String postResult(String url, StringEntity paramEntity, String contentType) throws IOException {
       String response = "";

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);

        CloseableHttpResponse postResp = null;
        try {
            httpPost.setHeader(HttpHeaders.CONTENT_TYPE, contentType);
            httpPost.setEntity(paramEntity);
            postResp = httpclient.execute(httpPost);

            HttpEntity entity = postResp.getEntity();

            if(entity != null){
                response = EntityUtils.toString(entity);
            }

            EntityUtils.consume(entity);
        } catch (Exception e) {
            logger.error("--- Http Post Error: ---", e);
        } finally {
            if(postResp != null){
                postResp.close();
            }
            return response;
        }
    }

}
