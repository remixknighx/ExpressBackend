package com.bill.service;

import com.bill.entity.Student;
import com.googlecode.jsonrpc4j.JsonRpcService;

/**
 * @author wangjf
 * @date 2018/4/6 0006.
 */
@JsonRpcService("/RpcService")
public interface RpcService {

    public String sayHello(String name, String content);

    public Student getStudent(Long studentId);

}
