package com.bill.service.impl;

import com.bill.dao.TestDao;
import com.bill.entity.Student;
import com.bill.service.RpcService;
import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author wangjf
 * @date 2018/4/6 0006.
 */
@AutoJsonRpcServiceImpl
public class RpcServiceImpl implements RpcService {

    @Autowired
    TestDao testDao;

    @Override
    public String sayHello(String name, String content) {
        return "name：" + name + "，content：" + content;
    }

    @Override
    public Student getStudent(Long studentId) {
        return testDao.queryStudentById(studentId);
    }

}
