package com.bill.service.impl;

import com.bill.dao.TestDao;
import com.bill.entity.Student;
import com.bill.service.TestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author wangjf
 * @date 2018/1/7 0007.
 */
@Service
@Transactional(value = "transactionManager", isolation = Isolation.REPEATABLE_READ)
public class TestServiceImpl implements TestService{

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    TestDao testDao;

    @Override
    public int insertStudent(Student student) {

        logger.info("修改前学生信息为：" + testDao.queryStudentById(student.getId()));

        int result = testDao.updateStudent(student);

        System.out.println("修改后学生信息为：" + testDao.queryStudentById(student.getId()));;

        return result;
    }

    @Override
    public Student queryStudentByName(String name) {
        return testDao.queryStudentByName(name);
    }

    @Override
    public Student queryStudentById(Long id) {
        Student student = testDao.queryStudentById(id);
        System.out.println(">>>>>>>>>>>>> " + student.toString());
        return student;
    }
}
