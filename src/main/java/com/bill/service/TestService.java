package com.bill.service;

import com.bill.entity.Student;

/**
 * @author wangjf
 * @date 2018/1/7 0007.
 */
public interface TestService {

    int insertStudent(Student student);

    Student queryStudentByName(String name);

    Student queryStudentById(Long id);

}
