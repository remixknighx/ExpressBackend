package com.bill.web;

import com.bill.common.exception.TestException;
import com.bill.dto.CommonResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author wangjf
 * @date 2018/1/20 0020.
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(TestException.class)
    public CommonResponse<Object> handleTestException(){
        return new CommonResponse<Object>(false, "自定义异常");
    }

    @ExceptionHandler(Exception.class)
    public CommonResponse<Object> handleGlobalException(){
        return new CommonResponse<Object>(false, "全局异常");
    }

}
