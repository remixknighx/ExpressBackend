package com.bill.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author wangjf
 * @date 2018/2/10 0010.
 */
@Controller
@RequestMapping("/")
public class GlobalController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String redirectSwagger(){
        return "redirect:/swagger-ui.html";
    }

}
