package com.bill.web;

import com.bill.dto.CommonResponse;
import com.bill.entity.Student;
import com.bill.service.TestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author wangjf
 * @date 2018/1/7 0007.
 */
@RestController
@RequestMapping("/index")
@Api(value = "index", tags = "测试控制器")
public class TestController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    TestService testService;

    @ApiOperation(value = "/student", notes="查询学生信息", tags = "tags测试", response = CommonResponse.class, httpMethod = "GET")
    @RequestMapping(value = "/student", method = RequestMethod.GET)
    public CommonResponse<Student> queryStudent(@RequestParam("studentId") Long studentId) {
        try{
            return new CommonResponse<Student>(testService.queryStudentById(studentId));
        }catch (Exception e){
            logger.error("查询ID为{}的学生信息失败!", studentId);
            return new CommonResponse<Student>(false, e.getMessage());
        }
    }

    @ApiOperation(value = "/studentInfo", notes="根据学生姓名查询学生信息", tags = "tags测试", response = CommonResponse.class, httpMethod = "GET")
    @RequestMapping(value = "/studentInfo", method = RequestMethod.GET)
    public CommonResponse<Student> queryStudentByName(@RequestParam("studentName") String studentName) {
        try{
            return new CommonResponse<Student>(testService.queryStudentByName(studentName));
        }catch (Exception e){
            logger.error("查询姓名为{}的学生信息失败!", studentName);
            return new CommonResponse<Student>(false, e.getMessage());
        }
    }

    /**
     * 接收application/json格式数据
     * */
    @RequestMapping(value = "/insertStudent", method = RequestMethod.POST, consumes = "application/json")
    public CommonResponse<Integer> insertStudent(@RequestBody Student student){
        try{
            return new CommonResponse<Integer>(testService.insertStudent(student));
        }catch (Exception e){
            logger.error("插入学{}的信息失败！", student.getName());
            return new CommonResponse<Integer>(false, e.getMessage());
        }
    }

    /**
     * 接收x-www-form-urlencoded数据
     * */
    @RequestMapping(value = "/insertNewStudent", method = RequestMethod.POST)
    public CommonResponse<Integer> insertNewStudent(Student student){
        try{
            return new CommonResponse<Integer>(testService.insertStudent(student));
        }catch (Exception e){
            logger.error("更新学生信息失败！", student.getName());
            return new CommonResponse<Integer>(false, e.getMessage());
        }
    }

}
