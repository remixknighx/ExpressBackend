package com.bill.config;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 启动swagger
 *
 * @author wangjf
 * @date 2018/1/20 0020.
 */
@EnableSwagger2
public class ApplicationSwaggerConfig {
}
