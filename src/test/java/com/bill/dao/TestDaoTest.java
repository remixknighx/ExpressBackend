package com.bill.dao;

import com.bill.entity.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * 配置spring和junit整合，junit启动时加载springIOC容器
 * spring-test, junit
 *
 * @author wangjf
 * @date 2018/1/7 0007.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/spring-dao.xml"})
public class TestDaoTest {

    @Autowired
    private TestDao testDao;

    @Test
    public void testInsertStudent() {
        Student student = new Student();
        student.setName("wang");
        student.setGender(1);
        student.setStatus(true);

        testDao.insertStudent(student);
    }

    @Test
    public void testQueryStudentById() {
        Student student = testDao.queryStudentById(1L);
        System.out.println(student.toString());
    }
}