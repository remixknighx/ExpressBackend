package com.bill.common.util;

import com.bill.entity.Student;
import org.junit.Test;
import springfox.documentation.spring.web.json.Json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * @author wangjf
 * @date 2018/3/25 0025.
 */
public class JsonUtilTest {

    @Test
    public void testConvertList2Json() throws Exception {
        List<Student> studentList = new ArrayList<Student>();
        for(int i = 0; i < 3; i++){
            Student student = new Student();
            student.setName("wang" + i).setGender(i % 2 == 0? 0: 1);
            studentList.add(student);
        }
        System.out.println("list to json>>>>" + JsonUtil.convertList2Json(studentList));
    }

    @Test
    public void testConvertJson2List() throws Exception {
        List<Student> studentList = new ArrayList<Student>();
        for(int i = 0; i < 3; i++){
            Student student = new Student();
            student.setName("wang" + i).setGender(i % 2 == 0? 0: 1);
            studentList.add(student);
        }
        String jsonList = JsonUtil.convertList2Json(studentList);

        System.out.println("json to List>>>> " + JsonUtil.convertJson2List(jsonList).size());
    }

    @Test
    public void testConvertMap2Json() throws Exception {
        Map<Object, Object> map = new HashMap<>();
        map.put("one", 1);
        map.put(2, true);
        System.out.println("map to json>>>> " + JsonUtil.convertMap2Json(map));
    }

    @Test
    public void testConvertJson2Map() throws Exception {
        Map<Object, Object> map = new HashMap<>();
        map.put("one", 1);
        map.put(2, true);
        Student student = new Student();
        student.setName("will");
        map.put("student", student);
        String jsonMap = JsonUtil.convertMap2Json(map);
        System.out.println("json to map>>>> " + JsonUtil.convertJson2Map(jsonMap).get("student"));
    }

    @Test
    public void testConvertObject2Json() throws Exception {
        Student student = new Student();
        student.setName("biil").setStatus(true);
        System.out.println("Object to json>>>>" + JsonUtil.convertObject2Json(student));
    }

    @Test
    public void testConvertJson2Object() throws Exception {
        Student student = new Student();
        student.setName("biil").setStatus(true);
        String objectJson = JsonUtil.convertObject2Json(student);
        System.out.println("json to object>>>>>>" + JsonUtil.convertJson2Object(objectJson));
    }

}