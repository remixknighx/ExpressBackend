package com.bill.common.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * @author wangjf
 * @date 2018/2/4 0004.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:redis-config.xml"})
public class RedisUtilTest {

    @Autowired
    RedisUtil redisUtil;

    //============== STRING ==============//
    @Test
    public void testGet() throws Exception {
        assertEquals("wangjf", redisUtil.get("name"));
    }

    @Test
    public void testHmset(){
        Map<String, Object> personInfo = new HashMap<String, Object>();
        personInfo.put("a","nn");
        personInfo.put("b","ddd");
        personInfo.put("c","sss");
        redisUtil.hmset("wangjf", personInfo);
    }

    @Test
    public void testSet() throws Exception {
        assertTrue(redisUtil.set("name", "wangjf"));
    }

    @Test
    public void testDel(){
        redisUtil.del("name", "mykey");
    }

    @Test
    public void testHasKey(){
        assertTrue(redisUtil.hasKey("wangjf"));
    }

    //============== HASH ==============//
    @Test
    public void testHget(){
        assertEquals("nn", redisUtil.hget("wangjf", "a"));
    }

    @Test
    public void testHmget(){
        System.out.println(redisUtil.hmget("wangjf"));
    }

    @Test
    public void testHdel(){
        redisUtil.hdel("wangjf", "a", "b");
    }

    @Test
    public void testHhasKey(){
        assertTrue(redisUtil.hHasKey("wangjf", "c"));
    }

    //========= SET =============//
    @Test
    public void testSset(){
        redisUtil.sSet("wjf", "111", "222", "333");
    }

    @Test
    public void testSGet(){
        System.out.println(redisUtil.sGet("wjf"));
    }

    @Test
    public void testSHasKey(){
        assertTrue(redisUtil.sHasKey("wjf", "111"));
    }

    @Test
    public void testGetSetSize(){
        assertEquals(3L, redisUtil.sGetSetSize("wjf"));
    }

    @Test
    public void testSetRemove(){
        assertEquals(1L, redisUtil.setRemove("wjf", "111"));
    }

    //============= LIST ================//
    @Test
    public void testLset(){
        redisUtil.lSet("wjfList2", "333");
        redisUtil.lSet("wjfList2", "444");
        redisUtil.lSet("wjfList2", "555");
    }

    @Test
    public void testLGetIndex(){
        System.out.println(redisUtil.lGetIndex("wjfList", 1L));
    }

    @Test
    public void testLGet(){
        System.out.println(redisUtil.lGet("wjfList2", 0L, 2L));
    }

    @Test
    public void testLGetListSize(){
        assertEquals(3L, redisUtil.lGetListSize("wjfList2"));
    }

    @Test
    public void testLUpdateIndex(){
        redisUtil.lUpdateIndex("wjfList", 0L, "eeee");
    }

}