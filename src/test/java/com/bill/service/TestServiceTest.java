package com.bill.service;

import com.bill.entity.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * @author wangjf
 * @date 2018/1/7 0007.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
        "classpath:spring/spring-dao.xml",
        "classpath:spring/spring-service.xml"
})
public class TestServiceTest {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    TestService testService;

    @Test
    public void testInsertStudent() throws Exception {
        Student student = new Student();
        student.setName("bill");
        student.setGender(0);
        student.setStatus(false);

        testService.insertStudent(student);
    }

    @Test
    public void testQueryStudentById(){
        testService.queryStudentById(29L);
    }
}