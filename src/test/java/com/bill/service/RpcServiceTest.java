package com.bill.service;

import com.bill.entity.Student;
import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.*;

/**
 * @author wangjf
 * @date 2018/4/6 0006.
 */
public class RpcServiceTest {

    @Test
    public void testSayHello() throws Throwable {
        JsonRpcHttpClient client = new JsonRpcHttpClient(new URL("http://localhost:8080/express/RpcService"));
        String result = client.invoke("sayHello", new Object[]{"王五", "说hi"}, String.class);
        System.out.println("result>>>>" + result);
    }

    @Test
    public void testGetStudent() throws Throwable {
        JsonRpcHttpClient client = new JsonRpcHttpClient(new URL("http://localhost:8080/express/RpcService"));
        Student result = client.invoke("getStudent", new Object[]{29}, Student.class);
        System.out.println("result>>>>" + result.toString());
    }

}