package com.bill.web;

import com.alibaba.fastjson.JSONObject;
import com.bill.common.constant.HttpConstant;
import com.bill.common.util.HttpUtil;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.bill.common.util.HttpUtil.postResult;

/**
 * @author wangjf
 * @date 2018/3/17 0017.
 */
public class HttpUtilTest {
    @Test
    public void testGetResult() throws Exception {
        String url = "http://localhost:8080/express/index/studentInfo?studentName=王剑峰";
        System.out.println(HttpUtil.getResult(url));
    }

    @Test
    public void testPostFormParam() throws IOException {
        String url = "http://localhost:8080/express/index/insertNewStudent";

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", "王剑峰");
        params.put("gender", 1);
        params.put("status", 1);

        System.out.println("post result:" + HttpUtil.postFormParam(url, params));
    }

    @Test
    public void testPostJsonResult() throws Exception {
        String url = "http://localhost:8080/express/index/insertStudent";

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", " 新建房");
        params.put("gender", 1);
        params.put("status", 1);

        JSONObject  obj = new JSONObject();
        obj.putAll(params);

        System.out.println("post result:" + HttpUtil.postJsonResult(url, obj.toJSONString()));
    }

}